import React from 'react'

class Footer extends React.Component {
    render() {
        return (
            <div id="footer">
                <div className="inner">
                    <ul className="icons">
                        <li><a href="https://twitter.com/_Max73" target="_blank" className="icon fa-twitter"><span className="label">Twitter</span></a></li>
                        <li><a href="https://gitlab.com/maxime.simplon" target="_blank" className="icon fa-gitlab"><span className="label">Gitlab</span></a></li>
                        <li><a href="https://www.linkedin.com/in/maxime-chofflet-a18635180/" target="_blank" className="icon fa-linkedin"><span className="label">Linkedin</span></a></li>
                        <li><a href="mailto:maxime.chofflet@gmail.com" className="icon fa-envelope-o"><span className="label">Email</span></a></li>
                    </ul>
                    <ul className="copyright">
                        <li>&copy; 2019 Maxime Chofflet</li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Footer
