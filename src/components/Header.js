import React from 'react'

import Footer from './Footer'
import avatar from '../assets/images/avatar.png'

class Header extends React.Component {
    render() {
        return (
            <header id="header">
                <div className="inner">
                    <a href="#" className="image avatar"><img src={avatar} alt="" /></a>
                    <h1><strong>Portfolio de Maxime Chofflet</strong></h1><h1> Développeur web JAMstack
                    </h1>
                </div>
                <Footer />
            </header>
        )
    }
}

export default Header
