import React from "react"
import Layout from '../components/layout'

export default () => (
    <Layout>
   <center>
   <title>Envoi réussi</title>
       <div class="main">
         <div class="card">
           <div class="header">
             <h1>Merci !</h1>
           </div>
           <div class="body">
   
             <p>Votre message a bien été envoyé.</p>
   
             <p>
               <a id="back-link" href="/">← Retour au site</a>
             </p>
           </div>
         </div>
       </div></center></Layout>)
     