import React from 'react'
import Helmet from 'react-helmet'

import Layout from '../components/layout'
// import Lightbox from 'react-images'
import Gallery from '../components/Gallery'

import thumb01 from '../assets/images/thumbs/cvpreview.png'
import thumb02 from '../assets/images/thumbs/webwinpreview.png'

import full01 from '../assets/images/fulls/cvpreview.png'
import full02 from '../assets/images/fulls/webwinpreview.png'



const DEFAULT_IMAGES = [
    { id: '1', src: full01, thumbnail: thumb01, caption: 'Mon CV en ligne', description: 'CV réalisé en JAMstack avec GatsbyJS en ReactJS.'},
];
const DEFAULT_IMAGES2 = [
{ id: '2', src: full02, thumbnail: thumb02, caption: 'Projet site web', description: 'Site web réalisé en JAMstack avec GatsbyJS en ReactJS.'},
];

class HomeIndex extends React.Component {

    constructor() {
        super();

        this.state = {
            lightboxIsOpen: false,
            currentImage: 0,
        };

        this.closeLightbox = this.closeLightbox.bind(this);
        this.gotoNext = this.gotoNext.bind(this);
        this.gotoPrevious = this.gotoPrevious.bind(this);
        this.openLightbox = this.openLightbox.bind(this);
        this.handleClickImage = this.handleClickImage.bind(this);
    }

    openLightbox (index, event) {
        event.preventDefault();
        this.setState({
            currentImage: index,
            lightboxIsOpen: true,
        });
    }
    closeLightbox () {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false,
        });
    }
    gotoPrevious () {
        this.setState({
            currentImage: this.state.currentImage - 1,
        });
    }
    gotoNext () {
        this.setState({
            currentImage: this.state.currentImage + 1,
        });
    }
    handleClickImage () {
        if (this.state.currentImage === this.props.images.length - 1) return;

        this.gotoNext();
    }

    render() {
        const siteTitle = "Portfolio de Maxime"
        const siteDescription = "Mon travail en tant que développeur web"

        return (
            <Layout>
                <Helmet>
                        <title>{siteTitle}</title>
                        <meta name="description" content={siteDescription} />
                </Helmet>

                <div id="main">

                    <section id="one">
                        <header className="major">
                            <h2>Présentation</h2>
                        </header>
                        <p>Formé au développement web full-stack à l'école du numérique Simplon, je vous présente mon portfolio. Mon nom est Maxime, je suis développeur junior avec un fort attrait pour la JAMstack (JavaScript, APIs, Markup). Cette approche révolutionne la notion de site statique en utilisant des technologies serverless pour la partie back-end. Les avantages sont nombreux aussi bien pour le développeur que pour le client. Du code propre en ReactJS avec GatsbyJS par exemple. Un hébergement gratuit et au potentiel surprenant avec l'outil Netlify. Au final le site est rapide, fluide, optimisé et sécurisé.    </p>
                        <ul className="actions">
                            <li><a href="https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/" target="_blank" className="button">En savoir +</a></li>
                        </ul>
                    </section>

                    <section id="two">
                        <h2>Mes réalisations</h2>

                        <Gallery images={DEFAULT_IMAGES.map(({ id, src, thumbnail, caption, description }) => ({
                            id,
                            src,
                            thumbnail,
                            caption,
                            description,
                            
                        }))} />

                        <ul className="actions">
                            <li><a href="https://maximechofflet-cv.netlify.com" target="_blank" className="button">Voir</a></li></ul>
                        
                            <Gallery images={DEFAULT_IMAGES2.map(({ id, src, thumbnail, caption, description }) => ({
                            id,
                            src,
                            thumbnail,
                            caption,
                            description,
                            
                        }))} />

                        <ul className="actions">
                           <li><a href="https://www.web-win.dev" target="_blank" className="button">Voir</a></li>
                        </ul>
                    </section>

                    <section id="three">
                        <h2>Me contacter</h2>
                        <p>N'hésitez pas à m'envoyer un message, je réponds rapidement. </p>
                        <div className="row">
                            <div className="8u 12u$(small)">
                            <form name="contact" method="post" action="/success" data-netlify="true" data-netlify-honeypot="bot-field">
          <input type="hidden" name="bot-field" />
          <input type="hidden" name="form-name" value="contact" />
                                    <div className="row uniform 50%">
                                        <div className="6u 12u$(xsmall)"><input type="text" name="name" id="name" placeholder="Nom" /></div>
                                        <div className="6u 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
                                        <div className="12u"><textarea name="message" id="message" placeholder="Message" rows="4"></textarea></div>
                                    </div>
                                <ul className="actions">
                                    <li><input type="submit" value="Envoyer" /></li>
                                </ul>
                                </form>
                            </div>
                            <div className="4u 12u$(small)">
                                <ul className="labeled-icons">
                                    <li>
                                        <h3 className="icon fa-home"><span className="label">Address</span></h3>
                                        29Av. Jean Jaurès<br />
                                        73000, Chambéry<br />
                                        France
                                    </li>
                                    <li>
                                        <h3 className="icon fa-mobile"><span className="label">Tél</span></h3>
                                        06 36 57 09 56
                                    </li>
                                    <li>
                                        <h3 className="icon fa-envelope-o"><span className="label">Email</span></h3>
                                        <a href="#">maxime.chofflet@gmail.com</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>

                </div>

            </Layout>
        )
    }
}

export default HomeIndex